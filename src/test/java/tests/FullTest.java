package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FullTest extends BaseClass{
		
	@Test
	public void fullTestIndoor() {
		try {
			
			// Clicking on full indoor test

			WebDriverWait wait = new WebDriverWait(driver,10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.agence3pp:id/id_test_scenario_full")));
			driver.findElement(By.id("com.agence3pp:id/id_test_scenario_full")).click();
			driver.findElement(By.xpath("//android.widget.ImageView[@content-desc=\"Here's an image or an icon\"]")).click();
			WebDriverWait wait1 = new WebDriverWait(driver,10);
			wait1.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.agence3pp:id/test_bottom_sheet_1")));
			driver.findElement(By.id("com.agence3pp:id/test_bottom_sheet_1")).click();

			// Waiting for results screen
			WebDriverWait wait2 = new WebDriverWait(driver, 240);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Results\"]")));

			driver.findElement(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Explanations\"]")).click();
			Thread.sleep(3000);

			// Printing results
			String download = driver.findElement(By.xpath("//android.view.ViewGroup[1]/android.widget.TextView[2]")).getText();
			System.out.println("Download result is: "+download);
			String upload = driver.findElement(By.xpath("//android.view.ViewGroup[2]/android.widget.TextView[2]")).getText();
			System.out.println("Upload result is: "+upload);
			String stream = driver.findElement(By.xpath("//android.view.ViewGroup[3]/android.widget.TextView[2]")).getText();
			System.out.println("Stream result is: "+stream);
			String web = driver.findElement(By.xpath("//android.view.ViewGroup[4]/android.widget.TextView[2]")).getText();
			System.out.println("Web result is: "+web);
			driver.navigate().back();
			WebDriverWait wait4 = new WebDriverWait(driver,10);
			wait4.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.agence3pp:id/navigation_2")));

			// Navigating back to history page
			driver.findElement(By.id("com.agence3pp:id/navigation_2")).click();
			WebDriverWait wait3 = new WebDriverWait(driver, 10);
			wait3.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[1]//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]")));
			driver.findElement(By.xpath("//android.widget.FrameLayout[1]//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]")).click();
			Thread.sleep(5000);
			if(driver.findElement(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Results\"]")).isDisplayed()) {
				Assert.assertTrue(true);
			}
		}catch(Exception exp) {
			System.out.println("Cause is: "+exp.getCause());
			System.out.println("Message is: "+exp.getMessage());
			System.out.println(exp.getStackTrace());
			exp.printStackTrace();
			Assert.assertTrue(false);
		}
	}
}