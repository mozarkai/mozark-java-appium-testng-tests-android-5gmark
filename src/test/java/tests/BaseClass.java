package tests;

import java.io.File;
import java.io.FileReader;
import java.net.URL;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class BaseClass {
	AppiumDriver<MobileElement> driver;

	@BeforeMethod
	public void setup() {
		try {
			DesiredCapabilities caps = null;

			String filePath = System.getenv("DESIRED_CAPABILITIES_FILE_PATH");
			if(filePath == null || filePath.equals("") || filePath.equals("null")) {
				caps = new DesiredCapabilities();
			}else {
				File jsonFile = new File(filePath);

				System.out.println(System.getenv("DESIRED_CAPABILITIES_FILE_PATH"));
				System.out.println(jsonFile.getCanonicalPath());
				Object obj = new JSONParser().parse(new FileReader(jsonFile)); 
				JSONObject jsonObject = (JSONObject) obj;
				caps = new DesiredCapabilities(); 

				for (Object key : jsonObject.keySet()) {
					String keyStr = (String) key;
					Object keyvalue = jsonObject.get(keyStr);

					if (!(keyvalue instanceof JSONObject)) {
						System.out.println(keyStr + ", " + keyvalue);
					}
					if (keyvalue instanceof JSONObject) {
						System.out.println((JSONObject) keyvalue);
					}
					caps.setCapability(keyStr,keyvalue);
				}
			}
			driver = new AppiumDriver<MobileElement> (new URL("http://127.0.0.1:4723/wd/hub"), caps);

		}catch(Exception e) {
			System.out.println("Cause is: "+e.getCause());
			System.out.println("Message is: "+e.getMessage());
			e.getStackTrace();
		}
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}
}