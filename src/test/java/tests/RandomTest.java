package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RandomTest extends BaseClass{
	
	@Test
	public void changeDataUnits() {

		try {

			// Clicking on account tab and changing data unit

			WebDriverWait wait = new WebDriverWait(driver,10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.agence3pp:id/navigation_5")));
			driver.findElement(By.id("com.agence3pp:id/navigation_5")).click();
			WebDriverWait wait1 = new WebDriverWait(driver,10);
			wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[1]//android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.TextView[1]")));
			driver.findElement(By.xpath("//android.widget.FrameLayout[1]//android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.TextView[1]")).click();
			WebDriverWait wait2 = new WebDriverWait(driver,10);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.CheckedTextView[4]")));
			driver.findElement(By.xpath("//android.widget.CheckedTextView[4]")).click();

			// Executing speed indoor test
			WebDriverWait wait3 = new WebDriverWait(driver,10);
			wait3.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.agence3pp:id/navigation_3")));
			driver.findElement(By.id("com.agence3pp:id/navigation_3")).click();
			driver.findElement(By.id("com.agence3pp:id/id_test_scenario_speed")).click();
			driver.findElement(By.xpath("//android.widget.ImageView[@content-desc=\"Here\'s an image or an icon\"]")).click();
			WebDriverWait wait4 = new WebDriverWait(driver,10);
			wait4.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.agence3pp:id/test_bottom_sheet_1")));
			driver.findElement(By.id("com.agence3pp:id/test_bottom_sheet_1")).click();

			// Waiting for results screen
			WebDriverWait wait5 = new WebDriverWait(driver, 240);
			wait5.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Results\"]")));

			driver.findElement(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Explanations\"]")).click();
			Thread.sleep(3000);

			// Printing results
			String download = driver.findElement(By.xpath("//android.view.ViewGroup[1]/android.widget.TextView[2]")).getText();
			System.out.println("Download result is: "+download);
			String upload = driver.findElement(By.xpath("//android.view.ViewGroup[2]/android.widget.TextView[2]")).getText();
			System.out.println("Upload result is: "+upload);
			driver.navigate().back();

			// Navigating back to history page
			driver.findElement(By.id("com.agence3pp:id/navigation_2")).click();
			WebDriverWait wait6 = new WebDriverWait(driver, 240);
			wait6.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[1]//android.view.ViewGroup[1]/android.widget.TextView[3]")));

			// Validating units in history tab
			String DownlinkResult = driver.findElement(By.xpath("//android.widget.FrameLayout[1]//android.view.ViewGroup[1]/android.widget.TextView[3]")).getText();
			try {
				if((DownlinkResult.matches("([0-9]+(\\.[0-9]+)) mBps"))) {
					Assert.assertTrue(true);
				}else {
					System.out.println("Result in history tab doesn't match to the current units");
				}
			}catch(Exception e) {
				e.getStackTrace();
			}	

			// Reverting data unit settings
			System.out.println("Reverting data unit settings");
			driver.findElement(By.id("com.agence3pp:id/navigation_5")).click();
			WebDriverWait wait7 = new WebDriverWait(driver,10);
			wait7.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[1]//android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.TextView[1]")));
			driver.findElement(By.xpath("//android.widget.FrameLayout[1]//android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.TextView[1]")).click();
			WebDriverWait wait8 = new WebDriverWait(driver,10);
			wait8.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.CheckedTextView[2]")));
			driver.findElement(By.xpath("//android.widget.CheckedTextView[2]")).click();
		}catch(Exception exp) {
			System.out.println("Cause is: "+exp.getCause());
			System.out.println("Message is: "+exp.getMessage());
			System.out.println("TestCase:Change data units-TestResult:Failed");
			exp.printStackTrace();
			Assert.assertTrue(false);
		}
	}

	@Test
	public void speedOutdoorTest() {
		try {

			// Clicking on speed outdoor test
			WebDriverWait wait3 = new WebDriverWait(driver,10);
			wait3.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.agence3pp:id/navigation_3")));
			driver.findElement(By.id("com.agence3pp:id/navigation_3")).click();
			driver.findElement(By.id("com.agence3pp:id/id_test_scenario_speed")).click();
			driver.findElement(By.xpath("//android.widget.ImageView[@content-desc=\"Here\'s an image or an icon\"]")).click();
			WebDriverWait wait4 = new WebDriverWait(driver,10);
			wait4.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.agence3pp:id/test_bottom_sheet_2")));
			driver.findElement(By.id("com.agence3pp:id/test_bottom_sheet_2")).click();

			// Waiting for results screen
			WebDriverWait wait5 = new WebDriverWait(driver, 240);
			wait5.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Results\"]")));

			driver.findElement(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Explanations\"]")).click();
			Thread.sleep(3000);

			// Printing results
			String download = driver.findElement(By.xpath("//android.view.ViewGroup[1]/android.widget.TextView[2]")).getText();
			System.out.println("Download result is: "+download);
			String upload = driver.findElement(By.xpath("//android.view.ViewGroup[2]/android.widget.TextView[2]")).getText();
			System.out.println("Upload result is: "+upload);
			driver.navigate().back();
			Assert.assertTrue(true);
		}catch(Exception exp) {
			System.out.println("Cause is: "+exp.getCause());
			System.out.println("Message is: "+exp.getMessage());
			System.out.println("TestCase:Speed outdoor test-TestResult:Failed");
			exp.printStackTrace();
			Assert.assertTrue(false);

		}

	}
}